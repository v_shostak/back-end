package my.project.backend.controller;

import my.project.backend.dto.AchievementFilter;
import my.project.backend.dto.AchievementPutDto;
import my.project.backend.dto.AchievementReadDto;
import my.project.backend.service.AchievementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Viacheslav Shostak
 */
@RestController
@RequestMapping("/api/v1")
public class AchievementsRestController {

    @Autowired
    private AchievementService service;

    @GetMapping("/achievements")
    public List<AchievementReadDto> getAchievements(AchievementFilter filter) {
        return service.findByFilter(filter);
    }

    @PutMapping("/achievements")
    public void updateAchievement(@RequestBody AchievementPutDto putDto) {
        service.updateAchievement(putDto);
    }
}
