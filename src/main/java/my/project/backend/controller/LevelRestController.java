package my.project.backend.controller;

import my.project.backend.dto.LevelCreateDto;
import my.project.backend.dto.LevelReadDto;
import my.project.backend.dto.UserCreateDto;
import my.project.backend.dto.UserReadDto;
import my.project.backend.service.LevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Viacheslav Shostak
 */
@RestController
@RequestMapping("/api/v1")
public class LevelRestController {

    @Autowired
    LevelService levelService;

    @GetMapping("/levels/{id}")
    LevelReadDto getLevel(@PathVariable Long id) {
        return levelService.getLevel(id);
    }

    @PostMapping("/levels")
    LevelReadDto createLevel(@RequestBody LevelCreateDto createDto) {
        return levelService.createLevel(createDto);
    }
}
