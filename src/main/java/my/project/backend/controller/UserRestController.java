package my.project.backend.controller;

import my.project.backend.dto.UserCreateDto;
import my.project.backend.dto.UserReadDto;
import my.project.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Viacheslav Shostak
 */
@RestController
@RequestMapping("/api/v1")
public class UserRestController {

    @Autowired
    UserService userService;

    @GetMapping("/users/{id}")
    UserReadDto getUser(@PathVariable Long id) {
        return userService.getUser(id);
    }

    @PostMapping("/users")
    UserReadDto createUser(@RequestBody UserCreateDto createDto) {
        return userService.createUser(createDto);
    }
}
