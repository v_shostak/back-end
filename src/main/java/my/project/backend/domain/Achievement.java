package my.project.backend.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.keyvalue.annotation.KeySpace;

/**
 * @author Viacheslav Shostak
 */
@Getter
@Setter
@EqualsAndHashCode
@KeySpace("achievements")
public class Achievement {
    @Id
    private Long id;
    private User user;
    private Level level;
    private int result;
}
