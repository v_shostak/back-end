package my.project.backend.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.keyvalue.annotation.KeySpace;

/**
 * @author Viacheslav Shostak
 */

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@KeySpace("levels")
public class Level {
    @Id
    private Long id;
}
