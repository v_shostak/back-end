package my.project.backend.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.keyvalue.annotation.KeySpace;

/**
 * @author Viacheslav Shostak
 */

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@KeySpace("users")
public class User {
    @Id
    private Long id;
}
