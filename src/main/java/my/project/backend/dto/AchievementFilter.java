package my.project.backend.dto;

import lombok.Data;

@Data
public class AchievementFilter {
    private Integer limit = 25;
    private Integer offset = 0;
    private Long userId;
    private Long levelId;
    private Integer minResult;
    private Integer maxResult;
}
