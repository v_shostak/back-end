package my.project.backend.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * @author Viacheslav Shostak
 */
@Data
public class AchievementPutDto {

    @JsonProperty("user_id")
    @NotNull
    private Long userId;

    @JsonProperty("level_id")
    @NotNull
    private Long levelId;

    @NotNull
    private Integer result;
}
