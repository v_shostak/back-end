package my.project.backend.dto;

import lombok.Data;

/**
 * @author Viacheslav Shostak
 */
@Data
public class LevelReadDto {
    private Long id;
}
