package my.project.backend.dto;

import lombok.Data;

/**
 * @author Viacheslav Shostak
 */
@Data
public class UserReadDto {
    private Long id;
}
