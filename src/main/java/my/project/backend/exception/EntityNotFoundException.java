package my.project.backend.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Viacheslav Shostak
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(Class entityClass, Long id) {
        super(String.format("Entity %s with id=%s is not found!", entityClass.getSimpleName(), id));
    }
}