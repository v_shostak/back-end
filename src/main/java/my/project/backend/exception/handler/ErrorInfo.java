package my.project.backend.exception.handler;

import lombok.Data;
import org.springframework.http.HttpStatus;

/**
 * @author Viacheslav Shostak
 */
@Data
public class ErrorInfo {
    private final HttpStatus httpStatus;
    private final Class exceptionClass;
    private final String message;
}