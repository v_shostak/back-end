package my.project.backend.repository;

import my.project.backend.domain.Achievement;
import my.project.backend.dto.AchievementFilter;

import java.util.List;

/**
 * @author Viacheslav Shostak
 */
public interface AchievementCustomRepository {
    List<Achievement> findByFilter(AchievementFilter filter);
}
