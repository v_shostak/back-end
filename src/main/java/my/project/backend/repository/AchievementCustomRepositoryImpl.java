package my.project.backend.repository;

import my.project.backend.domain.Achievement;
import my.project.backend.dto.AchievementFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.keyvalue.core.KeyValueTemplate;
import org.springframework.data.keyvalue.core.query.KeyValueQuery;

import java.util.List;

/**
 * @author Viacheslav Shostak
 */

public class AchievementCustomRepositoryImpl implements AchievementCustomRepository {

    @Autowired
    private KeyValueTemplate keyValueTemplate;

    @Override
    public List<Achievement> findByFilter(AchievementFilter filter) {

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("1==1 ");

        Sort sort = Sort.by(Sort.Direction.DESC, "result");

        if (filter.getUserId() != null) {
            queryBuilder.append(String.format("And user.id == %s ", filter.getUserId()));
            sort.and(Sort.by(Sort.Direction.DESC, "level.id"));
        }

        if (filter.getLevelId() != null) {
            queryBuilder.append(String.format("And level.id == %s ", filter.getLevelId()));
            sort.and(Sort.by(Sort.Direction.DESC, "user.id"));
        }

        if (filter.getMinResult() != null) {
            queryBuilder.append(String.format("And result >= %s ", filter.getMinResult()));
        }

        if (filter.getMaxResult() != null) {
            queryBuilder.append(String.format("And result < %s ", filter.getMaxResult()));
        }

        KeyValueQuery<String> query = new KeyValueQuery<>(queryBuilder.toString());

        query.setSort(sort);

        query.setOffset(filter.getOffset());
        query.limit(filter.getLimit());

        List<Achievement> achievements = (List<Achievement>) keyValueTemplate.find(query, Achievement.class);

        return achievements;
    }
}
