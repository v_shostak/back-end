package my.project.backend.repository;

import my.project.backend.domain.Achievement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Viacheslav Shostak
 */

@Repository
public interface AchievementRepository extends CrudRepository<Achievement, Long>, AchievementCustomRepository {
}
