package my.project.backend.repository;

import my.project.backend.domain.Level;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Viacheslav Shostak
 */

@Repository
public interface LevelRepository extends CrudRepository<Level, Long> {
}
