package my.project.backend.repository;

import my.project.backend.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Viacheslav Shostak
 */

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
