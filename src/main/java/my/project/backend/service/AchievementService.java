package my.project.backend.service;

import my.project.backend.dto.AchievementFilter;
import my.project.backend.dto.AchievementPutDto;
import my.project.backend.dto.AchievementReadDto;

import java.util.List;

/**
 * @author Viacheslav Shostak
 */

public interface AchievementService {
    List<AchievementReadDto> findByFilter(AchievementFilter filter);
    void updateAchievement(AchievementPutDto putDto);
}
