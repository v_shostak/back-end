package my.project.backend.service;

import my.project.backend.dto.LevelCreateDto;
import my.project.backend.dto.LevelReadDto;

/**
 * @author Viacheslav Shostak
 */
public interface LevelService {
    LevelReadDto createLevel(LevelCreateDto createDto);
    LevelReadDto getLevel(Long id);
}
