package my.project.backend.service;

import my.project.backend.domain.Achievement;
import my.project.backend.domain.Level;
import my.project.backend.domain.User;
import my.project.backend.dto.*;
import org.springframework.stereotype.Service;

/**
 * @author Viacheslav Shostak
 */
@Service
public class TranslationService {

    public UserReadDto toReadDto(User user){
        UserReadDto readDto = new UserReadDto();
        readDto.setId(user.getId());
        return readDto;
    }

    public User toEntity(UserCreateDto createDto){
        User user = new User();
        return user;
    }

    public LevelReadDto toReadDto(Level level){
        LevelReadDto readDto = new LevelReadDto();
        readDto.setId(level.getId());
        return readDto;
    }

    public Level toEntity(LevelCreateDto createDto){
        Level level = new Level();
        return level;
    }

    public AchievementReadDto toReadDto(Achievement achievement){
        AchievementReadDto readDto = new AchievementReadDto();
        readDto.setResult(achievement.getResult());
        readDto.setUserId(achievement.getUser().getId());
        readDto.setLevelId(achievement.getLevel().getId());
        return readDto;
    }

    public Achievement toEntity(AchievementPutDto putDto) {
        Achievement achievement = new Achievement();

        User user = new User();
        user.setId(putDto.getUserId());
        achievement.setUser(user);

        Level level = new Level();
        level.setId(putDto.getLevelId());
        achievement.setLevel(level);

        achievement.setResult(putDto.getResult());

        return achievement;
    }
}
