package my.project.backend.service;

import my.project.backend.domain.User;
import my.project.backend.dto.UserCreateDto;
import my.project.backend.dto.UserReadDto;

/**
 * @author Viacheslav Shostak
 */
public interface UserService {

    UserReadDto createUser(UserCreateDto createDto);

    UserReadDto getUser(Long id);
}
