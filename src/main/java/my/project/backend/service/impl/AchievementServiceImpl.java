package my.project.backend.service.impl;

import my.project.backend.domain.Achievement;
import my.project.backend.domain.Level;
import my.project.backend.domain.User;
import my.project.backend.dto.AchievementFilter;
import my.project.backend.dto.AchievementPutDto;
import my.project.backend.dto.AchievementReadDto;
import my.project.backend.exception.EntityNotFoundException;
import my.project.backend.repository.AchievementRepository;
import my.project.backend.repository.LevelRepository;
import my.project.backend.repository.UserRepository;
import my.project.backend.service.AchievementService;
import my.project.backend.service.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Viacheslav Shostak
 */

@Service
public class AchievementServiceImpl implements AchievementService {

    @Autowired
    private AchievementRepository repository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LevelRepository levelRepository;

    @Autowired
    TranslationService translationService;

    @Override
    public List<AchievementReadDto> findByFilter(AchievementFilter filter) {

        List<Achievement> achievements = repository.findByFilter(filter);

        return achievements.stream()
                .map(translationService::toReadDto).collect(Collectors.toList());
    }


    @Override
    public void updateAchievement(AchievementPutDto putDto) {
        User user = getUserRequired(putDto.getUserId());
        Level level = getLevelRequired(putDto.getLevelId());

        Achievement achievement = new Achievement();
        achievement.setUser(user);
        achievement.setLevel(level);
        achievement.setResult(putDto.getResult());

        repository.save(achievement);
    }

    private Level getLevelRequired(Long id) {
        return levelRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException(Level.class, id)
        );
    }

    private User getUserRequired(Long id) {
        return userRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException(User.class, id)
        );
    }
}
