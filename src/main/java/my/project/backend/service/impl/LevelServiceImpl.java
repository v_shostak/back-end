package my.project.backend.service.impl;

import my.project.backend.domain.Level;
import my.project.backend.dto.LevelCreateDto;
import my.project.backend.dto.LevelReadDto;
import my.project.backend.exception.EntityNotFoundException;
import my.project.backend.repository.LevelRepository;
import my.project.backend.service.LevelService;
import my.project.backend.service.TranslationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Viacheslav Shostak
 */
@Service
public class LevelServiceImpl implements LevelService {

    @Autowired
    private LevelRepository repository;

    @Autowired
    TranslationService translationService;

    @Override
    public LevelReadDto createLevel(LevelCreateDto createDto) {
        Level level = translationService.toEntity(createDto);
        level.setId(repository.count() +1);
        level = repository.save(level);
        return translationService.toReadDto(level);
    }

    @Override
    public LevelReadDto getLevel(Long id) {
        Level level = getRequired(id);
        return translationService.toReadDto(level);
    }

    private Level getRequired(Long id) {
        return repository.findById(id).orElseThrow(() ->
                new EntityNotFoundException(Level.class, id)
        );
    }
}
