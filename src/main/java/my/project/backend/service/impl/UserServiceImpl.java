package my.project.backend.service.impl;

import my.project.backend.domain.User;
import my.project.backend.dto.UserCreateDto;
import my.project.backend.dto.UserReadDto;
import my.project.backend.exception.EntityNotFoundException;
import my.project.backend.repository.UserRepository;
import my.project.backend.service.TranslationService;
import my.project.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Viacheslav Shostak
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository repository;

    @Autowired
    TranslationService translationService;


    @Override
    public UserReadDto createUser(UserCreateDto createDto) {
        User user = translationService.toEntity(createDto);
        user.setId(repository.count() + 1);
        user = repository.save(user);
        return translationService.toReadDto(user);
    }

    @Override
    public UserReadDto getUser(Long id) {
        User user = getRequired(id);
        return translationService.toReadDto(user);
    }

    private User getRequired(Long id) {
        return repository.findById(id).orElseThrow(() ->
                new EntityNotFoundException(User.class, id)
        );
    }
}
