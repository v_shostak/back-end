package my.project.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import my.project.backend.domain.Achievement;
import my.project.backend.dto.AchievementPutDto;
import my.project.backend.exception.EntityNotFoundException;
import my.project.backend.service.AchievementService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Viacheslav Shostak
 */

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = AchievementsRestController.class)
public class AchievementRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AchievementService achievementService;


    @Test
    public void testUpdateAchievementByWrongLevelIdUserId() throws Exception{
        AchievementPutDto putDto = new AchievementPutDto();
        putDto.setLevelId(-1L);

        EntityNotFoundException exception = new EntityNotFoundException(Achievement.class, putDto.getUserId());
        Mockito.doThrow(exception).when(achievementService).updateAchievement(putDto);

        String responseResult = mockMvc.perform(put("/api/v1/achievements/")
                .content(objectMapper.writeValueAsString(putDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(responseResult.contains(exception.getMessage()));
    }


    @Test
    public void testUpdateAchievementByWrongUserId() throws Exception{
        AchievementPutDto putDto = new AchievementPutDto();
        putDto.setUserId(-1L);

        EntityNotFoundException exception = new EntityNotFoundException(Achievement.class, putDto.getUserId());
        Mockito.doThrow(exception).when(achievementService).updateAchievement(putDto);

        String responseResult = mockMvc.perform(put("/api/v1/achievements/")
                .content(objectMapper.writeValueAsString(putDto))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(responseResult.contains(exception.getMessage()));
    }


}
