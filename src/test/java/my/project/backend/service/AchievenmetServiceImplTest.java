package my.project.backend.service;

import my.project.backend.domain.Achievement;
import my.project.backend.domain.Level;
import my.project.backend.domain.User;
import my.project.backend.dto.AchievementFilter;
import my.project.backend.dto.AchievementPutDto;
import my.project.backend.dto.AchievementReadDto;
import my.project.backend.exception.EntityNotFoundException;
import my.project.backend.repository.AchievementRepository;
import my.project.backend.repository.LevelRepository;
import my.project.backend.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author Viacheslav Shostak
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AchievenmetServiceImplTest {

    @Autowired
    private AchievementService service;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private AchievementRepository repository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    LevelRepository levelRepository;


    private User userOne;
    private User userTwo;
    private Level levelOne;
    private Level levelTwo;
    private Level levelThree;

    @Before
    public void initData() {
        userOne = createUser();
        userTwo = createUser();

        levelOne = createLevel();
        levelTwo = createLevel();
        levelThree = createLevel();
    }

    @After
    public void clearData() {
        repository.deleteAll();
    }


    @Test
    public void testUpdateAchievement() {

        AchievementPutDto putDto = new AchievementPutDto();
        putDto.setLevelId(levelOne.getId());
        putDto.setUserId(userOne.getId());
        putDto.setResult(25);

        service.updateAchievement(putDto);

        AchievementFilter achievementFilter = new AchievementFilter();
        List<AchievementReadDto> UserAchievementByUser = service.findByFilter(achievementFilter);

        Assert.assertTrue(UserAchievementByUser.size() == 1);
        AchievementReadDto achievementReadDto = UserAchievementByUser.get(0);
        Assertions.assertThat(UserAchievementByUser.get(0)).isEqualToComparingFieldByField(putDto);

    }

    @Test(expected = EntityNotFoundException.class)
    public void testUpdateAchievementByWrongUserId() {

        AchievementPutDto putDto = new AchievementPutDto();
        putDto.setLevelId(-1L);
        putDto.setUserId(-1L);
        putDto.setResult(25);

        service.updateAchievement(putDto);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateAchievementByNullable() {

        AchievementPutDto putDto = new AchievementPutDto();
        service.updateAchievement(putDto);
    }


    @Test
    public void testFindAchievementByEmptyFilter() {
        Achievement ach1 = createAchievement(userOne, levelOne, 55);
        Achievement ach2 = createAchievement(userOne, levelTwo, 8);
        Achievement ach3 = createAchievement(userOne, levelThree, 15);
        Achievement ach4 = createAchievement(userTwo, levelThree, 22);

        AchievementFilter achievementFilter = new AchievementFilter();
        achievementFilter.setLimit(20);

        List<AchievementReadDto> UserAchievementByUser = service.findByFilter(achievementFilter);

        Assert.assertTrue(UserAchievementByUser.size() == 4);
        Assertions.assertThat(UserAchievementByUser)
                .containsExactlyInAnyOrder(
                        translationService.toReadDto(ach1),
                        translationService.toReadDto(ach3),
                        translationService.toReadDto(ach4),
                        translationService.toReadDto(ach2)
                );
    }


    @Test
    public void testFindAchievementByUserId() {

        Achievement ach1 = createAchievement(userOne, levelOne, 55);
        Achievement ach2 = createAchievement(userOne, levelTwo, 8);
        Achievement ach3 = createAchievement(userOne, levelThree, 15);
        Achievement ach4 = createAchievement(userTwo, levelThree, 22);

        AchievementFilter achievementFilter = new AchievementFilter();
        achievementFilter.setLimit(20);
        achievementFilter.setUserId(userOne.getId());

        List<AchievementReadDto> UserAchievementByUser = service.findByFilter(achievementFilter);

        Assert.assertTrue(UserAchievementByUser.size() == 3);
        Assertions.assertThat(UserAchievementByUser)
                .containsExactly(
                        translationService.toReadDto(ach1),
                        translationService.toReadDto(ach3),
                        translationService.toReadDto(ach2)
                );

    }


    @Test
    public void testFindAchievementByLevelId() {

        Achievement ach1 = createAchievement(userOne, levelOne, 55);
        Achievement ach2 = createAchievement(userOne, levelTwo, 8);
        Achievement ach3 = createAchievement(userOne, levelThree, 15);
        Achievement ach4 = createAchievement(userTwo, levelThree, 22);

        AchievementFilter achievementFilter = new AchievementFilter();
        achievementFilter.setLimit(20);
        achievementFilter.setLevelId(levelThree.getId());

        List<AchievementReadDto> UserAchievementByUser = service.findByFilter(achievementFilter);

        Assert.assertTrue(UserAchievementByUser.size() == 2);
        Assertions.assertThat(UserAchievementByUser)
                .containsExactly(
                        translationService.toReadDto(ach4),
                        translationService.toReadDto(ach3)
                );

    }


    @Test
    public void testFindAchievementByLevelIdAndUserId() {

        Achievement ach1 = createAchievement(userOne, levelOne, 55);
        Achievement ach2 = createAchievement(userOne, levelTwo, 8);
        Achievement ach3 = createAchievement(userOne, levelThree, 15);
        Achievement ach4 = createAchievement(userTwo, levelThree, 22);

        AchievementFilter achievementFilter = new AchievementFilter();
        achievementFilter.setLimit(20);
        achievementFilter.setUserId(userTwo.getId());
        achievementFilter.setLevelId(levelThree.getId());

        List<AchievementReadDto> UserAchievementByUser = service.findByFilter(achievementFilter);

        Assert.assertTrue(UserAchievementByUser.size() == 1);
        Assertions.assertThat(UserAchievementByUser)
                .containsExactly(
                        translationService.toReadDto(ach4)
                );

    }

    private User createUser() {
        User user = new User();
        user.setId(userRepository.count() + 1);
        return userRepository.save(user);
    }

    private Level createLevel() {
        Level level = new Level();
        level.setId(levelRepository.count() + 1);
        return levelRepository.save(level);
    }

    private Achievement createAchievement(User user, Level level, int result) {
        Achievement achievement = new Achievement();
        achievement.setUser(user);
        achievement.setLevel(level);
        achievement.setResult(result);
        return repository.save(achievement);
    }

}
