package my.project.backend.service;

import my.project.backend.domain.Level;
import my.project.backend.dto.LevelReadDto;
import my.project.backend.exception.EntityNotFoundException;
import my.project.backend.repository.LevelRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Viacheslav Shostak
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class LevelServiceTest {

    @Autowired
    private LevelService levelService;

    @Autowired
    private LevelRepository levelRepository;

    @Test(expected = EntityNotFoundException.class)
    public void testGetLevelByWrongId() {
        levelService.getLevel(-1L);
    }

    @Test
    public void testGetUser() {
        Level givenLevel = createLevel();
        LevelReadDto levelReadDto = levelService.getLevel(givenLevel.getId());

        Assertions.assertThat(levelReadDto).isEqualToComparingFieldByField(givenLevel);

        Level actualLevel = levelRepository.findById(levelReadDto.getId()).get();
        Assertions.assertThat(levelReadDto).isEqualToComparingFieldByField(actualLevel);

    }


    private Level createLevel() {
        Level level = new Level();
        level.setId(levelRepository.count() + 1);
        return levelRepository.save(level);
    }
}
