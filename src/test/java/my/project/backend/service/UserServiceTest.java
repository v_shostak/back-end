package my.project.backend.service;

import my.project.backend.domain.User;
import my.project.backend.dto.AchievementFilter;
import my.project.backend.dto.UserReadDto;
import my.project.backend.exception.EntityNotFoundException;
import my.project.backend.repository.UserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

/**
 * @author Viacheslav Shostak
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;


    @Test(expected = EntityNotFoundException.class)
    public void testGetUserByWrongId() {
        userService.getUser(-1L);
    }

    @Test
    public void testGetUser() {
        User givenUser = createUser();
        UserReadDto userReadDto = userService.getUser(givenUser.getId());

        Assertions.assertThat(userReadDto).isEqualToComparingFieldByField(givenUser);

        User actualUser = userRepository.findById(userReadDto.getId()).get();
        Assertions.assertThat(userReadDto).isEqualToComparingFieldByField(actualUser);

    }


    private User createUser() {
        User user = new User();
        user.setId(userRepository.count() + 1);
        return userRepository.save(user);
    }


}
